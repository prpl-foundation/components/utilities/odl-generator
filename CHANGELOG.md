# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v1.12.5 - 2024-12-06(15:11:33 +0000)

### Other

- [odl-generator] add support for failover IP interface

## Release v1.12.4 - 2024-09-27(08:37:13 +0000)

## Release v1.12.3 - 2024-09-09(08:23:19 +0000)

### Other

- [MoCA] Definition of MoCA's InterfaceName to 'eth5' leads to misconfiguration of bridge

## Release v1.12.2 - 2024-08-30(11:38:29 +0000)

### Other

- add support for multiple runlevels

## Release v1.12.1 - 2024-07-31(08:18:09 +0000)

### Other

- add support for multiple runlevels

## Release v1.12.0 - 2024-07-16(20:10:43 +0000)

### New

- trigger_runlevel_scripts.sh: add uci-defaults script to trigger runlevel specific scripts

### Other

- add support for multiple runlevels

## Release v1.11.0 - 2024-04-23(08:05:11 +0000)

### New

- add hasAnyUpstream helper function

## Release v1.10.0 - 2024-04-19(12:42:48 +0000)

### New

- process_odl_templates.uc: getUpstreamInterfaceIndex(): add support for a DefaultUpstream parameter

### Other

- examples: update example

## Release v1.9.0 - 2024-03-27(11:29:58 +0000)

## Release v1.8.0 - 2024-03-05(14:34:44 +0000)

### New

- Add getInterfaceType helper

## Release v1.7.0 - 2024-01-30(14:02:19 +0000)

### New

- Add support to get Radio Index from Alias

## Release v1.6.4 - 2024-01-11(13:29:15 +0000)

### New

- Add helper function to check if wireless interface is an endpoint

## Release v1.6.3 - 2023-11-20(10:10:53 +0000)

### Other

- [odl-generator] Add support for Moca interfaces

## Release v1.6.2 - 2023-10-12(08:10:20 +0000)

## Release v1.6.1 - 2023-10-09(11:11:51 +0000)

## Release v1.6.0 - 2023-08-28(12:41:12 +0000)

### New

- Add helper function to detect private vaps

## Release v1.5.4 - 2023-08-16(15:29:04 +0000)

### Other

- add pcb odl path file

## Release v1.5.3 - 2023-06-15(08:11:35 +0000)

### Other

- Opensource main branch of odl-generator

## Release v1.5.2 - 2023-06-06(12:55:23 +0000)

### Other

- detect radio devs and freqBands from uci

## Release v1.5.1 - 2023-03-31(14:18:21 +0000)

### Other

- Fix compatibility with newer ucode version

## Release v1.5.0 - 2023-03-24(09:26:42 +0000)

### New

- Add extra helper functions to extend wifi lowerlayers support

## Release v1.4.3 - 2023-03-02(12:57:29 +0000)

### Changes

- Move template files to the affected components

## Release v1.4.2 - 2023-02-08(14:18:14 +0000)

### Other

- [wld] rename default files to inject conf in approp order

## Release v1.4.1 - 2023-01-25(17:53:36 +0000)

### Changes

- [pwhm] use default dir iso one default file

## Release v1.4.0 - 2022-12-12(14:32:09 +0000)

### Other

- set SSID LowerLayers as TR181 compliant path

## Release v1.3.0 - 2022-12-12(13:22:44 +0000)

### New

- Issue: SSW-SSW-5515 [pwhm] use default dir iso one default file

## Release v1.2.2 - 2022-11-30(15:39:57 +0000)

### Changes

- Disable SSID by default

## Release v1.2.1 - 2022-11-28(08:50:24 +0000)

### Other

- clean up wld default to more coherent values

## Release v1.2.0 - 2022-11-14(17:16:27 +0000)

### Other

- - Setting defaults parameters in odl file for endpoint

## Release v1.1.2 - 2022-11-14(08:00:31 +0000)

### Fixes

- Remove tr181-qos files from install target

## Release v1.1.1 - 2022-11-10(13:39:42 +0000)

### Changes

- [tr181-qos] Remove component as it supports netmodel

## Release v1.1.0 - 2022-10-05(15:54:10 +0000)

### Other

- add wifi guest vaps

## Release v1.0.1 - 2022-09-22(14:13:00 +0000)

## Release proj_prpl_M1-2022_v0.2.5 - 2022-09-15(12:13:24 +0000)

### Other

- [prpl] set prplOS as default SSIDs and disable them by default

## Release proj_prpl_M1-2022_v0.2.4 - 2022-09-08(10:13:19 +0000)

### Other

- : [pwhm] detect radios by context and not by pci order

## Release proj_prpl_M1-2022_v0.2.3 - 2022-08-26(09:16:03 +0000)

### Other

- : [pwhm] detect radios by context and not by pci order

## Release proj_prpl_M1-2022_v0.2.2 - 2022-08-12(08:31:48 +0000)

### Fixes

- fix stability issue with pwhm amx

## Release proj_prpl_M1-2022_v0.2.1 - 2022-07-07(13:48:06 +0000)

### Other

- Opensource branch proj_prpl_M1-2022

## Release proj_prpl_M1-2022_v0.2.0 - 2022-06-21(12:31:57 +0000)

### New

- Use wld defaults odl generator iso config

## Release proj_prpl_M1-2022_v0.1.0 - 2022-06-21(07:21:55 +0000)

### New

- Align odls to M1 2022 accelplan delivery

## Release proj_prpl_M1-2022_v0.0.2 - 2022-06-10(11:36:02 +0000)

### Fixes

- Update artifacts

## Release proj_prpl_M1-2022_v0.0.1 - 2022-06-10(09:09:33 +0000)

## Release v0.5.0 - 2022-06-02(09:44:23 +0000)

### New

- Add cwmp_plugin template and add port for cwmpd_conn_req in firewall

## Release v0.4.1 - 2022-05-06(09:48:03 +0000)

### Fixes

- Update rate limits for tr181-qos

## Release v0.4.0 - 2022-04-27(12:51:07 +0000)

### New

- Update templates to be compatible with tr181-qos v1.1.9

## Release v0.3.0 - 2022-03-25(09:40:12 +0000)

### New

- Add templating for qos manager to handle various wan interfaces

## Release v0.2.2 - 2022-01-25(14:48:41 +0000)

### Fixes

- Align with ip-manager v1.11.1

## Release v0.2.1 - 2022-01-20(10:04:46 +0000)

### Fixes

- Aligning odl templates according to changes in ip-manager v1.11.0

## Release v0.2.0 - 2022-01-20(09:29:11 +0000)

### New

- Add wireless interfaces support including guest virtual access points

## Release v0.1.3 - 2022-01-14(15:55:47 +0000)

### Changes

- Aligning odl templates according to changes in ip-manager v1.10.0

## Release v0.1.2 - 2022-01-14(13:29:10 +0000)

### Fixes

- Renamed bridging guest default odl to align wth main TR181 bridging manager

## Release v0.1.1 - 2022-01-07(10:56:55 +0000)

### Other

- Opensource component

## Release v0.1.0 - 2022-01-06(15:52:28 +0000)

