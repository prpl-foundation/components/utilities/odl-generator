include makefile.inc

NOW = $(shell date +"%Y-%m-%d(%H:%M:%S %z)")

# Extra destination directories
PKGDIR = ./output/$(MACHINE)/pkg/

define create_changelog
	@$(ECHO) "Update changelog"
	mv CHANGELOG.md CHANGELOG.md.bak
	head -n 9 CHANGELOG.md.bak > CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	$(ECHO) "## Release $(VERSION) - $(NOW)" >> CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	$(GIT) log --pretty=format:"- %s" $$($(GIT) describe --tags | grep -v "merge" | cut -d'-' -f1)..HEAD  >> CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	tail -n +10 CHANGELOG.md.bak >> CHANGELOG.md
	rm CHANGELOG.md.bak
endef

# targets
all:

clean:

install: all
	$(INSTALL) -D -p -m 0755 files/build_default_odl.sh $(DEST)/etc/uci-defaults/99build_default_odl.sh
	$(INSTALL) -D -p -m 0755 files/netlayout_from_uci.sh $(DEST)/etc/uci-defaults/98netlayout_from_uci.sh
	$(INSTALL) -D -p -m 0755 files/process_odl_templates.uc $(DEST)/etc/amx/process_odl_templates.uc
	$(INSTALL) -D -p -m 0755 files/runlevel.sh $(DEST)/lib/functions/runlevel.sh
	$(INSTALL) -D -p -m 0755 files/keep_runlevel_odl.sh $(DEST)/etc/uci-defaults/98keep_runlevel_odl.sh
ifeq ($(CONFIG_SAH_ODLGEN_MULTIPLE_RUNLEVELS),y)
	$(INSTALL) -D -p -m 0755 files/trigger_runlevel_scripts.sh $(DEST)/etc/uci-defaults/01-trigger_runlevel_scripts.sh
endif

package: all
	$(INSTALL) -D -p -m 0755 files/build_default_odl.sh $(PKGDIR)/etc/uci-defaults/99build_default_odl.sh
	$(INSTALL) -D -p -m 0755 files/netlayout_from_uci.sh $(PKGDIR)/etc/uci-defaults/98netlayout_from_uci.sh
	$(INSTALL) -D -p -m 0755 files/process_odl_templates.uc $(PKGDIR)/etc/amx/process_odl_templates.uc
	$(INSTALL) -D -p -m 0755 files/runlevel.sh $(PKGDIR)/lib/functions/runlevel.sh
	$(INSTALL) -D -p -m 0755 files/keep_runlevel_odl.sh $(PKGDIR)/etc/uci-defaults/98keep_runlevel_odl.sh
ifeq ($(CONFIG_SAH_ODLGEN_MULTIPLE_RUNLEVELS),y)
	$(INSTALL) -D -p -m 0755 files/trigger_runlevel_scripts.sh $(PKGDIR)/etc/uci-defaults/01-trigger_runlevel_scripts.sh
endif
	cd $(PKGDIR) && $(TAR) -czvf ../$(COMPONENT)-$(VERSION).tar.gz .
	cp $(PKGDIR)../$(COMPONENT)-$(VERSION).tar.gz .
	make -C packages

changelog:
	$(call create_changelog)

.PHONY: all clean changelog install package