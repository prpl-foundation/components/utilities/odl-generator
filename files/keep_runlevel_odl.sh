#!/bin/sh
# Script removes all other runlevels and keeps the files for the current runlevel

source /lib/functions/runlevel.sh

ODL_PATH_AMX="/etc/amx"
ODL_PATH_LEGACY="/usr/lib"
RUNLEVEL="$(get_runlevel)"

for file in $(find "$ODL_PATH_AMX" "$ODL_PATH_LEGACY" -name "*.*.odl" -type f); do
    if [[ "$file" != *."$RUNLEVEL".odl ]]; then
    	rm "$file"
    else
        rm -f "${file%%.$RUNLEVEL.odl}".odl
        mv "$file" "${file%%.$RUNLEVEL.odl}".odl
    fi
done

for file in $(find "$ODL_PATH_AMX" "$ODL_PATH_LEGACY" -name "*.*.odl.uc" -type f); do
    if [[ "$file" != *."$RUNLEVEL".odl.uc ]]; then
    	rm "$file"
    else
        rm -f "${file%%.$RUNLEVEL.odl.uc}".odl.uc
        mv "$file" "${file%%.$RUNLEVEL.odl.uc}".odl.uc
    fi
done

for dir in $(find "$ODL_PATH_AMX" "$ODL_PATH_LEGACY" -name "*.*.d" -type d); do
    if [[ "$dir" != *."$RUNLEVEL".d ]]; then
        rm -rf "$dir"
    else
        rm -f "${dir%%.$RUNLEVEL.d}".d
        mv "$dir" "${dir%%.$RUNLEVEL.d}".d
    fi
done
