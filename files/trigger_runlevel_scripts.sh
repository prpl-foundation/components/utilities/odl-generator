#!/bin/sh
source /lib/functions/runlevel.sh

RUNLEVEL="$(get_runlevel)"

if [ -d "/etc/runlevel.d/${RUNLEVEL}" ]; then
    for file in /etc/runlevel.d/${RUNLEVEL}/*; do
        "$file"
    done
fi
