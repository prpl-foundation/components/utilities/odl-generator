#!/bin/sh
# Script used to generate a simple network layout description fro uci configuration
# It is intended to provide a default working layout description as input for defaut odl templates

NETLAYOUT=/etc/networklayout.json

. /usr/share/libubox/jshn.sh
. /lib/functions.sh

# Bridges Naming
brLan="br-lan"
brGuest="br-guest"
brLcm="br-lcm"

netlayout_add_wan_itf()
{
	local itf_name=$(uci -q get network.wan.ifname)
	[ -z "$itf_name" ] && itf_name=$(uci -q get network.wan.device)

	json_select Interfaces
	json_add_object 
	json_add_string Alias $(echo $itf_name |tr 'a-z' 'A-Z')
	json_add_string Name $itf_name
	json_add_string Type ethernet
	json_add_string Upstream true
	json_close_object
	json_select ..
}

netlayout_add_itf_description()
{
	local itf_alias=$1
	local itf_name=$2
	local itf_type=$3
	local itf_operFreq=$4
	local itf_ssid=$5

	json_select Interfaces	
	json_add_object 
	json_add_string Alias $itf_alias
	json_add_string Name $itf_name
	json_add_string Type $itf_type
	[ ! -z "$itf_operFreq" ] && json_add_string OperatingFrequency $itf_operFreq
	[ ! -z "$itf_ssid" ] && json_add_string SSID $itf_ssid
	json_close_object
	json_select ..
}

netlayout_add_radio_description()
{
	local rad_alias=$1
	local rad_operFreq=$2

	json_select Radios
	json_add_object
	json_add_string Alias $rad_alias
	json_add_string OperatingFrequency $rad_operFreq
	json_close_object
	json_select ..
}

netlayout_add_ep_itf_description()
{
	local itf_alias=$1
	local itf_name=$2
	local itf_operFreq=$3

	json_select Interfaces
	json_add_object
	json_add_string Alias $itf_alias
	json_add_string Name $itf_name
	json_add_string Type wireless
	json_add_string isEndpoint "true"
	json_add_string OperatingFrequency $itf_operFreq
	json_add_string BridgeInterface $brLan

	json_close_object
	json_select ..
}


netlayout_add_itf_to_bridge()
{
	local bridge_name=$1
	local itf_alias=$2
	local itf_name=$3

	json_select Bridges
	json_select $bridge_name
	json_select Ports
	json_add_object 
	json_add_string Alias $itf_alias
	json_add_string Name $itf_name 
	json_close_object
	json_select ..
	json_select ..
	json_select ..
}

netlayout_add_lan_bridged_itfs()
{
	local bridged=$(uci get network.lan.ifname)
	local bridged=$(uci -q get network.lan.ifname)
	[ -z "$bridged" ] && return
	
	#first add all lan interface description
	for itf in $bridged ; do
		netlayout_add_itf_description "$(echo $itf | tr 'a-z' 'A-Z')" "${itf}" "ethernet" 	
	done
	
	#then add all lan interfaces as bridge ports
	local idx=0
	for itf in $bridged; do
		netlayout_add_itf_to_bridge "Lan" "eth_port${idx}" "${itf}"
		idx=$((idx+1))
	done
}

netlayout_add_lan_dsa_bridged_itfs()
{
	local intf=$(uci -q get network.lan.device)
	[ -z "$intf" ] && return

	. /lib/functions.sh

	add_network_device()
	{
		local name
		local ports
		local cfg="$1"

		config_get name "$cfg" name
		config_get ports "$cfg" ports

		[ "$intf" != "$name" ] && return

		#first add all lan interface description
		for itf in $ports; do
			netlayout_add_itf_description "$(echo $itf | tr 'a-z' 'A-Z')"  "${itf}" "ethernet"
		done

		#then add all lan interfaces as bridge ports
		for itf in $ports; do
			netlayout_add_itf_to_bridge "Lan" "$(echo $itf | tr 'a-z' 'A-Z')"  "${itf}"
		done
	}

	config_load network
	config_foreach add_network_device device
}

netlayout_add_wireless_itf()
{
	local itf_name=$1 
	local linux_itf=$2
	local network=$3
	local operFreq=$4
	local ssid=$5

	#first add wireless interface description
	netlayout_add_itf_description "$(echo $itf_name | tr 'a-z' 'A-Z')" "${linux_itf}" "wireless" "${operFreq}" "${ssid}"

	#then if interface is part of lan or guest network add it to corresponding bridge
	if [ "${network}" = "lan" ] ; then
		netlayout_add_itf_to_bridge "Lan" "${itf_name}" "${linux_itf}"
		wlan_port_idx=$((wlan_port_idx+1))
	elif [ "${network}" = "guest" ] ; then
		netlayout_add_itf_to_bridge "Guest" "${itf_name}" "${linux_itf}"
		guest_wl_idx=$((guest_wl_idx+1))
	elif [ "${network}" = "lcm" ] ; then
		netlayout_add_itf_to_bridge "Lcm" "${itf_name}" "${linux_itf}"
	fi

	# if new radio add the corresponding endpoint entry
	if ! echo "${op_rad_list}" | grep -q "${operFreq}"; then
		ep_itf_name="ep${operFreq:0:1}g0"
		ep_linux_itf=$(echo "$linux_itf" | sed 's/-1/-2/; t; s/$/-2/') # static wlanIDX-2 
		netlayout_add_ep_itf_description "${ep_itf_name}" "${ep_linux_itf}" "${operFreq}"
		netlayout_add_itf_to_bridge "Lan" "${ep_itf_name}" "${ep_linux_itf}"
		wlan_port_idx=$((wlan_port_idx+1))
		op_rad_list="${op_rad_list};${operFreq}"
	fi
	
}

netlayout_add_wireless_radio()
{
	local name=$1
	local operFreq=$2
	netlayout_add_radio_description "${name}" "${operFreq}"
}

netlayout_add_wireless_entries()
{
	local type=$1
	shift;
	if [ "${type}" == "INTERFACE" ]; then
		netlayout_add_wireless_itf $*
	elif [ "${type}" == "RADIO" ]; then
		netlayout_add_wireless_radio $*
	fi
}

netlayout_add_wireless_itfs()
{
	wlan_port_idx=0
	guest_wl_idx=0
	op_rad_list=""
 	read -r -d '' awkscript << 'EOF'
 	function flush_vap(vap,radios,    radname,vapIdx,operFreq,radIdx){
		if ( vap["name"] == "" )  return;
	
		radname=vap["rad"];
		vapIdx = radios[radname"_nbVap"]++;
		operFreq = radios[radname"_band"];
		if ( vap["itf"] == "" ) { #build ifname if it was not provided as done in mac80211.sh 
			radIdx = radios[radname"_radIdx"];
			vap["itf"]= "wlan" radIdx;
			if (vapIdx != 0) vap["itf"]= vap["itf"] "-" vapIdx;
		}
	
		printf "INTERFACE" ";" vap["name"] ";" vap["itf"] ";" vap["net"] ";" operFreq ";" vap["ssid"] " ";
		delete vap;
	}
	function flush_rad(rad,radios,    operFreq){
		if ( rad == "" ) return;
		operFreq = radios[rad"_band"];
		printf "RADIO" ";" rad ";" operFreq " ";
	}
	BEGIN { FS="="; idxRad=0; currentVap["name"]=""; radname=""; } 
	/wireless/ {
		split($1,path,"."); gsub( "'","",$2);
		
		if ($2 == "wifi-device") {   # new radio section
			flush_rad(radname,arrRad);
			radname=path[2];
			arrRad[radname"_radIdx"]=idxRad++; 
			arrRad[radname"_nbVap"]=0;
		} 
		if ($2 == "wifi-iface")  {   # new interface section
			flush_vap(currentVap,arrRad);
			currentVap["name"] = path[2];
		}
		if (path[3] == "device") { currentVap["rad"]= $2; }  #get radio reference
		if (path[3] == "ifname") { currentVap["itf"]= $2; }  #get linux interface name 	
		if (path[3] == "network") { currentVap["net"]= $2; }  #get associated network  
		if (path[3] == "ssid") { currentVap["ssid"]= $2; }  #get ssid
		if (path[3] == "hwmode") {
			if ($2 == "11g") { arrRad[radname"_band"]="2.4GHz"; }
			if ($2 == "11a") { arrRad[radname"_band"]="5GHz"; }
			if ($2 == "11ax") { arrRad[radname"_band"]="6GHz"; }
		}
		if (path[3] == "band") {
			if ($2 == "2g" || $2 == "2.4GHz") { arrRad[radname"_band"]="2.4GHz"; }
			if ($2 == "5g" || $2 == "5GHz") { arrRad[radname"_band"]="5GHz"; }
			if ($2 == "6g" || $2 == "6GHz") { arrRad[radname"_band"]="6GHz"; }
		}
	}
	END { flush_vap(currentVap,arrRad); flush_rad(radname,arrRad); }
EOF
 
 	wifi_itfs=$(uci show wireless | awk "$awkscript" )
  	for itf in $wifi_itfs; do
		netlayout_add_wireless_entries ${itf//;/ }
	done 
}


netlayout_init() 
{
	json_init
	
	# Create json skeleton for interfaces and bridge definition
	json_add_array Interfaces
	json_close_array
	
	json_add_object Bridges
		json_add_object Lan
			json_add_string Name $brLan
			json_add_array Ports
			json_close_array
		json_close_object
		
		json_add_object Guest
			json_add_string Name $brGuest
			json_add_array Ports
			json_close_array
		json_close_object

		json_add_object Lcm
			json_add_string Name $brLcm
			json_add_array Ports
			json_close_array
		json_close_object		
	json_close_object
	json_add_array Radios
	json_close_array
}

wait_for_uci_wireless() {
	local phy_count
	local uci_count
	local timeout=3
	local elapsed=0
	local min_phy=0
	local timeout_uci=$(uci -q get wireless.vendor.phy_detect_timeout)
	local min_phy_uci=$(uci -q get wireless.vendor.min_phy)

	[ -n "$timeout_uci" ] && timeout=$timeout_uci
	[ -n "$min_phy_uci" ] && min_phy=$min_phy_uci

	phy_count=$(iw phy | grep -c Wiphy)
	uci_count=$(grep -c 'wifi-device.*radio' /etc/config/wireless 2>/dev/null || echo 0)

	while ([ "$phy_count" -lt "$min_phy" ] || [ "$phy_count" -ne "$uci_count" ]) && [ $elapsed -lt $timeout ]; do
		phy_count=$(iw phy | grep -c Wiphy)
		uci_count=$(grep -c 'wifi-device.*radio' /etc/config/wireless 2>/dev/null || echo 0)
		elapsed=$((elapsed + 1))
		sleep 1
	done
}

# if not present, generate a default network layout description from uci config
if [ ! -s $NETLAYOUT ]; then
	netlayout_init
	netlayout_add_wan_itf
	netlayout_add_lan_bridged_itfs
	netlayout_add_lan_dsa_bridged_itfs
	wait_for_uci_wireless
	netlayout_add_wireless_itfs

	json_dump -i > $NETLAYOUT
fi
