#!/bin/sh

get_runlevel() {
    local runlevel
    if [ -f "/perm/runlevel/mode" ]; then
        runlevel=$(cat /perm/runlevel/mode)
    elif [ -f "/etc/prplconfig" ]; then
        source /etc/prplconfig
        if [ -n "$CONFIG_ACCESSPOINT" ] && [ -z "$CONFIG_GATEWAY"]; then
            runlevel="ap"
        else
            runlevel="gw"
        fi
    else
        runlevel="gw"
    fi
    echo $runlevel
}
