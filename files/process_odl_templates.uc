{%-
//**************************************************************************************************************************
// This ucode script is intended to process odl templates and populate it using an input board json description
// ucode website: https://github.com/jow-/ucode/blob/master/README.md
//
// Typical usage:
// ./ucode -E BD=<Board.json> -e '{"ODL_UC_PATH":"<processed_template.odl.uc>"}' -i ./process_odl_templates.uc
//**************************************************************************************************************************


//***************** Helper functions  definition *****************
helper_funcs = {
	//Returns a boolean if given interface name is of wireless type
	isInterfaceWireless : function(Itf_name) {
		for ( let Itf in BD.Interfaces ) {
			if (Itf.Name ==  Itf_name  && Itf.Type == "wireless" ) { return true; }
		}
		return false;
	},
	//Returns a boolean if given interface name is of wireless Accesspoint
	isInterfaceWirelessAp : function(Itf_name) {
		for ( let Itf in BD.Interfaces ) {
			if (Itf.Name ==  Itf_name  && Itf.Type == "wireless" && Itf.isEndpoint != "true"  ) { return true; }
		}
		return false;
	},
	//Returns a boolean if given interface name is of wireless Endpoint
	isInterfaceWirelessEp : function(Itf_name) {
		for ( let Itf in BD.Interfaces ) {
			if (Itf.Name ==  Itf_name  && Itf.Type == "wireless" && Itf.isEndpoint == "true"  ) { return true; }
		}
		return false;
	},
	//Returns a boolean if given interface name is of moca type
	isInterfaceMoca : function(Itf_name) {
		for ( let Itf in BD.Interfaces ) {
			if (Itf.Name ==  Itf_name  && Itf.Type == "moca" ) { return true; }
		}
		return false;
	},
	//Returns a boolean if given interface name is of wds type
	getInterfaceType : function(Itf_name) {
		for ( let Itf in BD.Interfaces ) {
			if (Itf.Name ==  Itf_name) { return Itf.Type; }
		}
		return false;
	},
	//Returns a boolean if given interface name is in the guest bridge
	isInterfaceGuest : function(Itf_name) {
		for ( let Itf in BD.Bridges.Guest.Ports ) {
				if (Itf.Name ==  Itf_name ) { return true; }
		}
		return false;
	},
	//Returns a boolean if given interface name is in the Lan bridge
	isInterfaceLan : function(Itf_name) {
		for ( let Itf in BD.Bridges.Lan.Ports ) {
				if (Itf.Name ==  Itf_name ) { return true; }
		}
		return false;
	},
	//Returns the index of the DefaultUpstream interface in BD.Itnerfaces array from board description file.
	//Returns the first defined Upstream index in case DefaultUpstream is not defined
	getUpstreamInterfaceIndex: function() {
		for ( let i =0; i < length(BD.Interfaces ); ++i) {
			if (BD.Interfaces[i].DefaultUpstream == "true" )
				return i;
		}
		for (let i=0 ; i < length(BD.Interfaces ); ++i) {
			if (BD.Interfaces[i].Upstream == "true" )
				return i;
		}
		return -1;
	},

	//Returns the name of the DefaultUpstream interface in BD.Interfaces array from board description file.
	//Returns the first defined Upstream name in case DefaultUpstream is not defined
	getUpstreamInterfaceName: function() {
		for ( let Itf in BD.Interfaces ) {
			if (Itf.DefaultUpstream == "true" )
				return Itf.Name;
		}
		for ( let Itf in BD.Interfaces ) {
			if (Itf.Upstream == "true" )
				return Itf.Name;
		}
		return "";
	},

	//Returns the index of the Failover interface in BD.Interfaces array from board description file.
	getFailoverInterfaceIndex: function() {
		for ( let i =0; i < length(BD.Interfaces ); ++i) {
			if (BD.Interfaces[i].Failover == "true" )
				return i;
		}
		return -1;
	},

	//Returns the position of an interface within a specific type based on its name.
	//This position typically corresponds to the default instance number in the associated datamodel section.
	//If no interface with the matching name and type is found, it returns -1.
	//Type is "wireless" or "ethernet" or "moca"
	getInterfaceIndex: function(Itf_name, Type) {
		ret = -1;
		for ( let i =0; i < length(BD.Interfaces ); ++i) {
			if (BD.Interfaces[i].Type == Type) {
				ret += 1;
				if (BD.Interfaces[i].Name == Itf_name)
					return ret;
			}
		}
		return -1;
	},

	// returns true if there exists at least one upstream interface, returns false otherwise
	hasAnyUpstream: function() {
		for ( let Itf in BD.Interfaces) {
			if (Itf.DefaultUpstream == "true" || Itf.Upstream == "true")
				return true;
		}
		return false;
	},

	//Returns the index of the radio with the given frequency
	getRadioIndex: function(Radio_freq) {
		for ( let i =0; i < length(BD.Radios ); ++i) {
			if (BD.Radios[i].OperatingFrequency == Radio_freq)
				return i;
		}
		return -1;
	},

	//Returns the index of the radio with the given Alias
	getRadioIndexByAlias: function(Radio_alias) {
		for ( let i =0; i < length(BD.Radios ); ++i) {
			if (BD.Radios[i].Alias == Radio_alias)
				return i;
		}
		return -1;
	},

	//Print statement if condition is true. Mainly helps writing one liner and avoid if/endif blocks
	printConditional: function(iCond,strStatement) {
		if ( iCond ) { print(  strStatement ); }
	}
};



//***************** Processing  *****************


//Process ODL template by including it.
//local scope functions doesn't seem to be shared to included files (bug?), so we pass on the array as additional global variable
include(ODL_UC_PATH, {BDfn : helper_funcs } );
-%}
