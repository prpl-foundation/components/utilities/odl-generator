#!/bin/sh
# Script will process odl templates to create default odl files for TR181 components

source /lib/functions/runlevel.sh

RUNLEVEL="$(get_runlevel)"
NETLAYOUT=/etc/networklayout.json
if [ -f "/etc/networklayout.${RUNLEVEL}.json" ]; then
	NETLAYOUT=/etc/networklayout.${RUNLEVEL}.json
fi
UC_TEMPLATE_PROCESSOR=/etc/amx/process_odl_templates.uc 
ODL_PATH_AMX="/etc/amx"
ODL_PATH_LEGACY="/usr/lib"

#if no network layout provided exit early 
if [ ! -s "$NETLAYOUT" ]; then
        exit 
fi

# gather templates .odl.uc files from specified directories
TEMPLATES=$(find "$ODL_PATH_AMX" "$ODL_PATH_LEGACY" -name "*.odl.uc")

#process all found odl.uc file and create odl resulting file just next to it
for ODLTMPL in  $TEMPLATES; do 
	TARGETODL=$(echo $ODLTMPL |sed "s|\.odl\.uc|.odl|"); 
	if ucode | grep -q '== Usage =='; then
		ucode -E BD=$NETLAYOUT -e '{"ODL_UC_PATH":"'${ODLTMPL}'"}' -i $UC_TEMPLATE_PROCESSOR > $TARGETODL
	else
		ucode -T -F BD=$NETLAYOUT -D '{"ODL_UC_PATH":"'${ODLTMPL}'"}' $UC_TEMPLATE_PROCESSOR > $TARGETODL
	fi
done
